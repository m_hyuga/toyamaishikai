<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
   <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">


<?php get_sidebar('4'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/kenmin/title.jpg" alt="県民の皆様へ 県民の皆様への医療・健康についての情報を提供しています。" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">アーカイブ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<p style="font-size:12px; border-bottom:1px #9A9A9A solid; padding-bottom:1px;"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'toyamaishikai' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></p>
<?php endwhile; endif; ?>
<?php if(function_exists(‘wp_pagenavi’)) { wp_pagenavi(); } ?>
		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->

<div class="clr"></div>

<?php get_footer(); ?>

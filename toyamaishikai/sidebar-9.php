<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>


		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/member/menu-title.jpg" alt="会員専用ページ" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1855">お知らせ</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=209">理事会報告</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=220">県医師会行事</a></li>
		              <li>・医報とやま</li>
                      <li>　 &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=8906">会員の栄誉</a></li>
		              <li>　 &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=247">郡市医師会だより</a></li>
		              <li>　 &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=249">各科医会から</a></li>
		              <li>　 &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=1011">勤務医コーナー</a></li>
		              <!--li>・<a href="../member/tsuchi/index.html">通知コーナー（必見）</a></li-->
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=257">医学研究助成について</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1920">尊厳死・安楽死について</a></li>
		              <li>・<a href="http://www.toyama.med.or.jp/wp/load_image.php?pdf=shumakki.pdf" target="_blank">がんの終末期医療に関する申し合わせについて</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1910">富山県医師会個人情報保護に関するコーナー（必見）</a></li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
		        <div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
						<li id="archives" class="widget-container">
							<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
							<ul>
								<?php wp_get_archives( 'cat=9' ); ?>
							</ul>
						</li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">



<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



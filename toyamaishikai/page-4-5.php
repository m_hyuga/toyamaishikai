<?php
/*
Template Name: template4-5
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">

<?php get_sidebar('4'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/kenmin/title.jpg" alt="県民の皆様へ 県民の皆様への医療・健康についての情報を提供しています。" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="wpbox">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop-ishikai' );
			?>

		            </div><!--end  class="wpbox"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
        <p class="fl-l"><a href="http://www.med.or.jp/jma/nichii/tpp/" target="_blank"><img src="../../images/kenmin/iryouseido/bn-tpp.jpg" alt="TPP" width="344" height="59" /></a></p>
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

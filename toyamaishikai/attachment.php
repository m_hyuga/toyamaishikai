<?php
/**
 * The template for displaying attachments.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ お知らせ</div>
	<div id="main">
	
<?php get_sidebar(); ?>

			<div id="content" role="main">
		      <div id="contents">
		        <div class="textTp"><img src="../images/common/contents-bg-top.png" alt="" width="692" height="20" /></div>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">お知らせ</h3>
		              <p class="fl-r"><a href="#">≫一覧を見る</a></p>
		              <br class="clear" />
		            </div>
		            <div class="box">

			<?php
			/* Run the loop to output the attachment.
			 * If you want to overload this in a child theme then include a file
			 * called loop-attachment.php and that will be used instead.
			 */
			get_template_part( 'loop', 'attachment' );
			?>
		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

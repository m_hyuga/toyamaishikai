<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>
      <br class="clear" />
	</div><!--end id="main"-->


    </div><!--end id="container"-->
    <p class="TopTxt"><a href="#pagetop">ページ上部へ▲</a></p>
  </div><!--end id="wrapper" class="hfeed"-->
  <div id="footer">
    <!--div id="foot-bn"><a href="http://www.med.or.jp/" target="_blank"><img src="../images/common/bn-jma.jpg" alt="日本医師会" width="246" height="66" /></a><a href="http://www.toyama.med.or.jp/josei/joseisien.html" target="_blank"><img src="../images/common/bn-josei.jpg" alt="女性医師等支援相談窓口" width="246" height="66" class="ml15" /></a><a href="http://www.qq.pref.toyama.jp/" target="_blank"><img src="../images/common/bn-guide.jpg" alt="とやま医療情報ガイド" width="246" height="66" class="ml15" /></a></div-->

    <div id="foot-bn"><a href="http://www.med.or.jp/" target="_blank"><img src="../images/common/bn-jma.jpg" alt="日本医師会" width="234" height="63" /></a><a href="http://www.toyama.med.or.jp/wp/?page_id=5051"><img src="../images/common/bn-josei.jpg" alt="女性医師等支援相談窓口" width="234" height="63" class="ml5" /></a><a href="http://www.qq.pref.toyama.jp/" target="_blank"><img src="../images/common/bn-guide.jpg" alt="とやま医療情報ガイド" width="234" height="63" class="ml5" /></a><a href="http://www.toyama.med.or.jp/wp/?page_id=2229"><img src="../images/common/bn-kinmui.jpg" alt="勤務医の皆様へ" width="234" height="63" class="ml5" /></a></div>



    <div id="foot-navi">
        <div class="boxleft">公益社団法人富山県医師会　〒939-8222　富山市蜷川336番地　TEL:076-429-4466</div>
      <div class="boxright"> ｜ <a href="http://www.toyama.med.or.jp/wp/?page_id=1955">お問合せ</a> ｜ <a href="http://www.toyama.med.or.jp/wp/?page_id=1960">サイトマップ</a> ｜ <a href="http://www.toyama.med.or.jp/wp/?page_id=1973">個人情報保護規定</a> ｜<a href="http://www.toyama.med.or.jp/wp/?page_id=1978"> リンク集</a> ｜</div>
      <br class="clear" />
    </div>
    <div id="foot-txt">
      <div id="copy"> Copyright&copy;　Toyama Medical Association All rights reserved.</div>
    </div>
  </div><!--end  id="footer"-->
</div><!--end id="wrapper-bg"-->

<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */


?>
</body>
</html>

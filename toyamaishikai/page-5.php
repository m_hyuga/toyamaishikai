<?php
/*
Template Name: template5
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">

<?php get_sidebar('5-2'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/iryou/title.jpg" alt="医療機関の皆様へ" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="wpbox">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop-ishikai' );
			?>

		            </div><!--end  class="wpbox"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

<?php
/*
Template Name: template5_l
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞<a href="http://www.toyama.med.or.jp/wp/?page_id=2158">医療機関の皆様へ</a> ＞ <?php the_title(); ?></div>
	<div id="main">

			<div id="content_l" role="main">
		      <div id="contents_l">
		        <h2><img src="../images/iryou/title_l.png" alt="医療機関の皆様へ" width="960" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="wpbox_l">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop-ishikai' );
			?>

		            </div><!--end  class="wpbox"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

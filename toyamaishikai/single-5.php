<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
 
get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ <a href="../iryou/index.html">医療機関の皆様へ</a> ＞ お知らせ</div>
	<div id="main">

<?php get_sidebar('5'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/iryou/title.jpg" alt="医療機関の皆様へ" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">アーカイブ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">


			<?php
			/* Run the loop to output the post.
			 * If you want to overload this in a child theme then include a file
			 * called loop-single.php and that will be used instead.
			 */
			get_template_part( 'loop', 'single' );
			?>

		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->

<div class="clr"></div>
<?php get_footer(); ?>

<?php /* Template Name: joseisoudan-top */ get_header("josei_top"); ?>
	<div class="InnerBlock">
		<div id="MainBlock">
			<?php if (have_posts()) : ?>

			    <?php while (have_posts()) : the_post(); ?>
			    	<?php remove_filter('the_content', 'wpautop'); ?>
					<?php the_content(); ?>
			    <?php endwhile; ?>

			<?php endif; ?>
		</div>
<?php get_sidebar("josei"); ?>
	
<?php get_footer("josei"); ?>
<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/other/menu-title.jpg" alt="その他のコンテンツ" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
					<li class="cat-item cat-item-4 current-cat"><a href="http://www.toyama.med.or.jp/wp/?page_id=2137" title="県民の皆様へ に含まれる投稿をすべて表示">県民の皆様へ</a></li>
					<li class="cat-item cat-item-5"><a href="http://www.toyama.med.or.jp/wp/?page_id=2158" title="医療機関の皆様へ に含まれる投稿をすべて表示">医療機関の皆様へ</a></li>
					<li class="cat-item cat-item-6"><a href="http://www.toyama.med.or.jp/wp/?page_id=2166" title="女性医師等支援相談 に含まれる投稿をすべて表示">女性医師等支援相談</a></li>
					<li class="cat-item cat-item-7"><a href="http://www.toyama.med.or.jp/wp/?page_id=2174" title="富山県医師会 に含まれる投稿をすべて表示">富山県医師会</a></li>
					<li class="cat-item cat-item-9"><a href="http://www.toyama.med.or.jp/wp/?page_id=1855" title="会員専用 に含まれる投稿をすべて表示">会員専用</a></li>
			        </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		       
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



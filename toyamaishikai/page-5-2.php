<?php
/*
Template Name: template5-2
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">

<?php get_sidebar('5-2'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/iryou/title.jpg" alt="医療機関の皆様へ" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="wpbox">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop-ishikai' );
			?>

		            </div><end  class="wpbox">
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		        
        <p class="fl-l mb5"><a href="http://www.toyama.med.or.jp/wp/?page_id=167"><img src="../../images/iryou/sangyou/bn-annai.jpg" alt="日医認定産業医制度産業医学研修会のご案内" width="344" height="59" /></a></p>
        <p class="fl-r mb5"><a href="http://www.toyama.med.or.jp/wp/?page_id=1378"><img src="../../images/iryou/sangyou/bn-hayawakari.jpg" alt="改正労働安全衛生法の早わかり" width="344" height="59" /></a></p>
        <p class="clear"><a href="http://www.toyama.med.or.jp/wp/?page_id=1473"><img src="../../images/iryou/sangyou/bn-sangyou.jpg" alt="産業保健・健康スポーツ" width="692" height="59" /></a></p>
		        
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

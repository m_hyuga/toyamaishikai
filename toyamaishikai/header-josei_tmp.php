<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/j_style.css" media="all">
<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/print.css" media="print">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script>
$(function(){
    $("img[src*='_on']").addClass("current");

    $("img,input[type='image']").hover(function(){
        if ($(this).attr("src")){
            $(this).attr("src",$(this).attr("src").replace("_off.", "_on."));
        }
    },function(){
        if ($(this).attr("src") && !$(this).hasClass("current") ){
            $(this).attr("src",$(this).attr("src").replace("_on.", "_off."));
        }
    });
});
</script>

<script type="text/javascript">

 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-32709371-1']);
 _gaq.push(['_trackPageview']);

 (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();

</script>



<script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>
</head>
<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<script src="http://html5shiv-printshiv.googlecode.com/svn/trunk/html5shiv-printshiv.js"></script>
<![endif]-->
<body>
	<header id="GlobalHeader">
		<div class="InnerBlock">
			<h1><a href="http://www.toyama.med.or.jp/wp/?page_id=5051"><img src="<?php bloginfo('template_url'); ?>/images/common/logo.png" alt="公益社団法人富山県医師会 女性医師等支援相談"></a></h1>
		</div>
	</header>

	<nav id="TemplateNavigation">
		<div class="InnerBlock">
			<ul>
				<li><a href="http://www.toyama.med.or.jp/wp/?page_id=5103"><img src="<?php bloginfo('template_url'); ?>/images/common/nav_tpm_btn01_off.png" alt="講演会 研修会 情報"></a></li>
				<li><a href="http://www.toyama.med.or.jp/wp/?page_id=5086"><img src="<?php bloginfo('template_url'); ?>/images/common/nav_tpm_btn02_off.png" alt="勤務環境"></a></li>
				<li><a href="http://www.toyama.med.or.jp/wp/?page_id=5131"><img src="<?php bloginfo('template_url'); ?>/images/common/nav_tpm_btn03_off.png" alt="保育支援介護"></a></li>
				<li><a href="http://www.toyama.med.or.jp/wp/?page_id=5073"><img src="<?php bloginfo('template_url'); ?>/images/common/nav_tpm_btn04_off.png" alt="復職 再研修"></a></li>
				<li><a href="http://www.toyama.med.or.jp/wp/?page_id=5196"><img src="<?php bloginfo('template_url'); ?>/images/common/nav_tpm_btn05_off.png" alt="相談 窓口"></a></li>
			</ul>
		</div>
	</nav>
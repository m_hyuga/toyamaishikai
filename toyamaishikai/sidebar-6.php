<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/josei/menu-title.jpg" alt="女性医師等支援相談" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
            <ul>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=2166">お知らせ</a></li>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=1071">保育関連</a></li>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=1076">勤務環境</a></li>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=1082">復職、転職、再研修</a></li>
              <li>・介護関連</li>
              <li>・<a href="http://www.toyama.med.or.jp/pdf/josei/joseiishi_moushikomi.pdf" target="_blank">相談窓口申込用紙（PDF）</a></li>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=304">講演会のご案内</a></li>
              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=702">関連リンク</a></li>
            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
		        <div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
						<li id="archives" class="widget-container">
							<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
							<ul>
								<?php wp_get_archives( 'cat=6' ); ?>
							</ul>
						</li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">



<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



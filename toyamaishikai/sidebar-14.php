<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/other/menu-title.jpg" alt="その他のコンテンツ" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1955">お問い合わせ</a></li>
	    	          <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1960">サイトマップ</a></li>
	        	      <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1973">個人情報保護規定</a></li>
	            	  <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1978">リンク集</a></li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		       
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">

<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



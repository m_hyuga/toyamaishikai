<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ お知らせ</div>
	<div id="main">


<?php get_sidebar(); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <div class="textTp"><img src="../images/common/contents-bg-top.png" alt="" width="692" height="20" /></div>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">お知らせ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">


				<!--カテゴリタイトルが入ります "カテゴリー別アーカイブ: +カテゴリ名"-->
				<!--h1 class="page-title"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), '<span>' . single_cat_title( '', false ) . '</span>' );
				?></h1-->
				<!--カテゴリタイトルが入ります "カテゴリ名"-->
				<!--h1 class="page-title"><span>カテゴリ名：<?php single_cat_title(); ?></span></h1-->
				<?php
				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->

<div class="clr"></div>

<?php get_footer(); ?>

<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */


get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ <a href="http://www.toyama.med.or.jp/wp/?page_id=2174">富山県医師会</a> ＞ お知らせ</div>
	<div id="main">

<?php get_sidebar('7'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/ishikai/title.jpg" alt="富山県医師会について" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">お知らせ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">
			
			<?php
			/* Run the loop to output the posts.
			 * If you want to overload this in a child theme then include a file
			 * called loop-index.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'index' );
			?>
		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

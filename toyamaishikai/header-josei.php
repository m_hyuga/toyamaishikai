<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8" />
<title></title>
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/j_style.css" media="all">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
<script>
$(function(){
    $("img[src*='_on']").addClass("current");
 
    $("img,input[type='image']").hover(function(){
        if ($(this).attr("src")){
            $(this).attr("src",$(this).attr("src").replace("_off.", "_on."));
        }
    },function(){
        if ($(this).attr("src") && !$(this).hasClass("current") ){
            $(this).attr("src",$(this).attr("src").replace("_on.", "_off."));
        }
    });
});
</script>
</head>
<body>
	<header id="GlobalHeader">
		<div class="InnerBlock">
			<h1><img src="<?php bloginfo('template_url'); ?>/images/common/logo.png" alt="公益社団法人富山県医師会 女性医師等支援相談"></h1>
		</div>
	</header>
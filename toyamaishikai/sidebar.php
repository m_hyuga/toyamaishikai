<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
				<div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
					<li class="cat-item cat-item-4 current-cat"><a href="http://www.toyama.med.or.jp/wp/?page_id=2137" title="県民の皆様へ に含まれる投稿をすべて表示">県民の皆様へ</a></li>
					<li class="cat-item cat-item-5"><a href="http://www.toyama.med.or.jp/wp/?page_id=2158" title="医療機関の皆様へ に含まれる投稿をすべて表示">医療機関の皆様へ</a></li>
					<li class="cat-item cat-item-6"><a href="http://www.toyama.med.or.jp/wp/?page_id=2166" title="女性医師等支援相談 に含まれる投稿をすべて表示">女性医師等支援相談</a></li>
					<li class="cat-item cat-item-7"><a href="http://www.toyama.med.or.jp/wp/?page_id=2174" title="富山県医師会 に含まれる投稿をすべて表示">富山県医師会</a></li>
					<li class="cat-item cat-item-9"><a href="http://www.toyama.med.or.jp/wp/?page_id=1855" title="会員専用 に含まれる投稿をすべて表示">会員専用</a></li>
			        </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      
		      </div><!--end id="menu"-->

			<ul class="xoxo">



<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>

			




			
		<?php endif; // end primary widget area ?>
			
		</div><!-- #primary .widget-area -->

<?php
	// A second sidebar for widgets, just because.
	if ( is_active_sidebar( 'secondary-widget-area' ) ) : ?>

		
<?php endif; ?>

<?php
/*
Template Name: template4-3
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">

<?php get_sidebar('4'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/kenmin/title.jpg" alt="県民の皆様へ 県民の皆様への医療・健康についての情報を提供しています。" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="wpbox">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			get_template_part( 'loop-ishikai' );
			?>

		            </div><!--end  class="wpbox"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
        <p class="fl-l mr4 mb5"><a href="http://www.mext.go.jp/a_menu/kenko/hoken/08040804.htm" target="_blank"><img src="../../images/kenmin/kansensyou/bn-guideline.jpg" alt="学校における麻しんガイドライン" width="228" height="59" /></a></p>
        <p class="fl-l mb5"><a href="http://www.toyama.med.or.jp/wp/?page_id=1331"><img src="../../images/kenmin/kansensyou/bn-kekkaku.jpg" alt="結核ってどんな病気？" width="228" height="59" /></a></p>
        <p class="fl-r mb5"><a href="http://www.toyama.med.or.jp/wp/?page_id=1336"><img src="../../images/kenmin/kansensyou/bn-kanen.jpg" alt="C型肝炎ってどんな病気？" width="228" height="59" /></a></p>
        <p class="clear"><a href="http://www.toyama.med.or.jp/wp/?page_id=1326"><img src="../../images/kenmin/kansensyou/bn-kansensyou.jpg" alt="感染症情報" width="692" height="59" /></a></p>
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

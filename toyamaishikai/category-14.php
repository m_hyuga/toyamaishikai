<?php get_header("josei_tmp"); ?>


    	<div class="InnerBlock">
<div id="MainBlock">

<section class="CommonSection">
    <h1><img src="http://www.toyama.med.or.jp/wp/wp-content/uploads/2015/05/ttl.png" alt="応援メッセージ" class="alignnone size-full wp-image-5136" /></h1>
      <section class="UnderSection1">
    
    <p>様々な環境で、着実にキャリアを積み上げ活躍しておられます先生方から応援メッセージが紹介します。<br>
    仕事を継続しながら、家庭との両立に対するアドバイスやキャリア・アップへのモチベーション維持の秘訣など役立つヒントが必ず見つかるはずです。<br>
    是非、一人ひとり自分に合った「医師としてのキャリアデザイン」をみつけ、医師を諦めることなく継続してください。<br>
    </p>
    <br>
		
		<?php if(have_posts()):while(have_posts()):the_post(); ?>
		<!--ここから-->
		<div class="list_box">
			<p class="left_photo" style="background-image: url('<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>')"></p>
			<div class="txt_jyosei">
					<div class="jyosei_ttl">
					<h3><?php the_title(); ?></h3><p class="time"><?php the_time('Y.m.d'); ?></p>
					</div>
					<h4><?php echo nl2br(post_custom('fukudai')); ?></h4>
			<p><?php
				$theContentForPreSingle = mb_substr(strip_tags($post-> post_content), 0, 75);
				$theContentForPreSingle = preg_replace('/\［caption(.+?)\/caption\］/','',$theContentForPreSingle);
				$theContentForPreSingle = preg_replace('/\［gallery(.+?)\］/','',$theContentForPreSingle);
					echo $theContentForPreSingle;
					if(mb_strlen($theContentForPreSingle) >= 75){echo '...';};
				?></p>
			
			<a href="<?php the_permalink(); ?>"><img src="<?php bloginfo('template_url'); ?>/images/josei/ouen/bt01.jpg" alt="" class="bt_right" /></a>
			</div>
		</div>
		<?php endwhile;endif; ?>


		</section>
	</section>
	</div>
<?php get_sidebar("josei"); ?>

<?php get_footer("josei"); ?>

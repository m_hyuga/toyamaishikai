<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ <a href="../kinmui/index.html">勤務医の皆様へ</a> ＞ お知らせ</div>
	<div id="main">


<?php get_sidebar('13'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/kinmui/title.jpg" alt="勤務医の皆皆様へ" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">お知らせ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">

				<?php
				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->

<div class="clr"></div>

<?php get_footer(); ?>

<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/iryou/menu-title.jpg" alt="医療機関の皆様へ" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
			              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=2158">お知らせ</a></li>
			              <li><strong>・<a href="<?php bloginfo('url'); ?>/?page_id=1473">産業保健、健康スポーツ</a></strong><a href="<?php bloginfo('url'); ?>/?page_id=1473"></a><br />
			                &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=167" target="_blank">日医認定産業医制度産業医学<br />
			              　研修会のご案内</a><br />
			              &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=1378">改正労働安全衛生法の早わかり</a></li>
			              <!--<li>・<a href="<?php bloginfo('url'); ?>/?page_id=1384">富山県地域産業保健センター</a></li>-->
					<li>・<strong><a href="<?php bloginfo('url'); ?>/?page_id=251">学術講演会開催情報</a></strong><br />
			              <li><strong>・<a href="<?php bloginfo('url'); ?>/?page_id=1388">医療安全</a></strong><a href="<?php bloginfo('url'); ?>/?page_id=1388"></a><br />
						  &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=4206">研修会・講演会のご案内</a></li>
		                  &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=1395">富山県院内感染対策協議会について</a></li>
			              <li><strong>・<a href="http://www.toyama.med.or.jp/wp/?page_id=950">治験について</a></strong><a href="http://www.toyama.med.or.jp/wp/?page_id=950"></a><br />
		                  &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=1475">治験ってなに？</a></li>
			              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=207">研修医の皆様へ</a></li>
			              <li>・<a href="http://www.orca.med.or.jp/" target="_blank">ORCAプロジェクト</a></li>
                          <li>・<a href="<?php bloginfo('url'); ?>/?page_id=683">連携パス</a></li>
                          <li>・<a href="<?php bloginfo('url'); ?>/?page_id=3033">糖尿病対策事業</a></li>
<li>・<a href="<?php bloginfo('url'); ?>/?page_id=6893">各種書類・様式ダウンロード</a></li>
		            </ul>
		          </div>
		        </div>
                <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
		        <div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
						<li id="archives" class="widget-container">
							<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
							<ul>
								<?php wp_get_archives( 'cat=5' ); ?>
							</ul>
						</li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">


<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



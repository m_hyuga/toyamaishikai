<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/ishikai/menu-title.jpg" alt="女性医師等支援相談" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
		              <li>・<a href="../ishikai/index.html">お知らせ</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=488">会長あいさつ</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=506">富山県医師会の入会について</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=313">沿革、会員数、富山県医師会作成<br>　行動計画・指針など</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=315">執行部、代議員・予備代議員</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1506">医心伝心（医報とやま）</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=52">医会代表者</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1481">郡市医師会</a></li>
                      <li>・<a href="<?php bloginfo('url'); ?>/?page_id=657">日本医師会生涯教育制度</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=1485">アクセスマップ</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>/?page_id=317">ディスクロージャー</a></li>
<li>・<a href="<?php bloginfo('url'); ?>/?page_id=6893">各種書類・様式ダウンロード</a></li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
		        <div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
						<li id="archives" class="widget-container">
							<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
							<ul>
								<?php wp_get_archives( 'cat=7' ); ?>
							</ul>
						</li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">



<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<div id="primary" class="widget-area" role="complementary">
		      <div id="menu">
		        <p><img src="../images/kenmin/menu-title.jpg" alt="県民の皆様へ" width="272" height="53" /></p>
		        <div class="textBg">
		          <div class="textBox">
		            <ul>
		              <li>・<a href="http://www.toyama.med.or.jp/wp/?page_id=2137">お知らせ</a></li>
		              <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1269">子供の病気や怪我について</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1269"></a><br />
		                &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1288">予防接種と子供の健康</a><br />
		                  &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1292l">子供の流行性疾患</a><br />
		                &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1307">病原性大腸菌O157</a></li>
		              <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1320">予防接種について</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1320"></a><br />
		                &#9492; <a href="<?php bloginfo('url'); ?>/?page_id=688">予防接種の広域化の実施について</a><br /></li>

		                <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1326">感染症情報</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1326"></a><br />
		            &#9492; <a href="http://www.mext.go.jp/a_menu/kenko/hoken/08040804.htm" target="_blank">学校における麻しんガイドライン</a><br />
		            &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1331">結核ってどんな病気？</a><br />
		               &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1336">C型肝炎ってどんな病気</a></li>
		              <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1342">こころの病気</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1342"></a><br />
		              &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1347">「パニック障害」ってどんな病気？</a></li>
		              <li>・<a href="http://www.toyama.med.or.jp/kenmin/kahun/" target="_blank">花粉症情報</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>?page_id=1350">糖尿病情報</a></li>
		              <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1354">救急蘇生</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1354"></a><br />
		              &#9492; <a href="<?php bloginfo('url'); ?>?page_id=1359">AEDマップ</a></li>
		              <li>・<a href="<?php bloginfo('url'); ?>?page_id=1364">薬物乱用</a></li>
		              <li>・<a href="http://www.nosmoke55.jp/nicotine/clinic.html" target="_blank">禁煙外来、禁煙クリニック一覧</a></li>
		              <li>・<a href="http://www.qq.pref.toyama.jp/" target="_blank">とやま医療情報ガイド</a></li>
		              <li><strong>・<a href="<?php bloginfo('url'); ?>?page_id=1367">医療制度</a></strong><a href="<?php bloginfo('url'); ?>?page_id=1367"></a><br />
		              &#9492; <a href="http://www.med.or.jp/jma/nichii/tpp/" target="_blank">TPP</a><br />
		              </li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ start-->
		        <p class="wiget-archive-title"><img src="../images/common/menu-bg-top.png" width="272" height="20" /></p>
				<div class="wiget-archiveBg ">
		          <div class="wiget-archive">
		            <ul>
						<li id="archives" class="widget-container">
							<h3 class="widget-title"><?php _e( 'Archives', 'twentyten' ); ?></h3>
							<ul>
								<?php wp_get_archives( 'cat=4' ); ?>
							</ul>
						</li>
		            </ul>
		          </div>
		        </div>
		        <!--カテゴリ別年月アーカイブ end-->
		      </div><!--end id="menu"-->
			<ul class="xoxo">
			</ul>
<?php
	/* When we call the dynamic_sidebar() function, it'll spit out
	 * the widgets for that widget area. If it instead returns false,
	 * then the sidebar simply doesn't exist, so we'll hard-code in
	 * some default sidebar stuff just in case.
	 */
	if ( ! dynamic_sidebar( 'primary-widget-area' ) ) : ?>
	

			<li id="search" class="widget-container widget_search">
				
			</li>

			<li id="archives" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
				
			</li>




			<li id="meta" class="widget-container">
				<h3 class="widget-title"></h3>
				<ul>
					
					<li></li>
					
				</ul>
			</li>


		<?php endif; // end primary widget area ?>
			</ul><!--end class="xoxo"-->
		</div><!-- #primary .widget-area -->



		<div id="secondary" class="widget-area" role="complementary">
			<ul class="xoxo">
				
			</ul>
		</div><!-- #secondary .widget-area -->



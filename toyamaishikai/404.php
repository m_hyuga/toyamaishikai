<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ お知らせ</div>
	<div id="main">

<?php get_sidebar(); ?>
		<div id="content" role="main">
		      <div id="contents">
		        <div class="textTp"><img src="../images/common/contents-bg-top.png" alt="" width="692" height="20" /></div>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">お知らせ</h3>
		              <p class="fl-r"><a href="#">≫一覧を見る</a></p>
		              <br class="clear" />
		            </div>
		            <div class="box">

		          
			<div id="post-0" class="post error404 not-found">
				<h1 class="entry-title"><?php _e( 'Not Found', 'twentyten' ); ?></h1>
				<div class="entry-content">
					<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>
					<?php get_search_form(); ?>
				</div><!-- .entry-content -->
			</div><!-- #post-0 -->

		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
		</div><!-- #content -->
	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>
<div class="clr"></div>
<?php get_footer(); ?>

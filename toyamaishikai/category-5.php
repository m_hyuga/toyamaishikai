<?php
/**
 * The template for displaying Category Archive pages.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <div id="breadNavi"><a href="../index.html">HOME</a> ＞ <a href="../iryou/index.html">医療機関の皆様へ</a> ＞ お知らせ</div>
	<div id="main">


<?php get_sidebar('5'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="../images/iryou/title.jpg" alt="医療機関の皆様へ" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
		            <div class="title">
		              <h3 class="fl-l">アーカイブ</h3>
		              <br class="clear" />
		            </div>
		            <div class="box">

<?php if(have_posts()): while(have_posts()): the_post(); ?>
				<p style="font-size:12px; border-bottom:1px #9A9A9A solid; padding-bottom:1px;"><a href="<?php the_permalink(); ?>" title="<?php printf( esc_attr__( 'Permalink to %s', 'twentyten' ), the_title_attribute( 'echo=0' ) ); ?>" rel="bookmark"><?php the_title(); ?></a></p>
<?php endwhile; endif; ?>
<?php if(function_exists(‘wp_pagenavi’)) { wp_pagenavi(); } ?>

		            </div><!--end  class="box"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->

<div class="clr"></div>

<?php get_footer(); ?>

<?php
/*
Template Name: template11
*/
?>
<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
    <?php if(function_exists('jBreadCrumbAink')) { echo jBreadCrumbAink(); } ?>
	<div id="main">

<?php get_sidebar('11-2'); ?>
			<div id="content" role="main">
		      <div id="contents">
		        <h2><img src="/images/photo/title.jpg" alt="会員の写真展" width="692" height="123" /></h2>
		        <div class="textBg">
		          <div class="textBox">
                  <?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

                   <div class="title">
		              <h3><?php the_title(); ?></h3>
		            </div>
                   <div class="box">
                   <?php the_content(); ?>
              
<?php endwhile; // end of the loop. ?>

            </div>
            
		            <div class="wpbox">

			
		           
<table cellpadding="0" cellspacing="0">
		<?php $paged = get_query_var('paged'); ?>
		<?php query_posts("posts_per_page=-1&paged=".$paged) ; ?>
                    <?php while( have_posts() ) : the_post(); ?>
                   <?php if ( ! in_category('12')) continue; ?>
       
        <tr>
        <td align="center" width="120"><?php if ( $first_image = catch_that_image() ) : ?><a href="<?php the_permalink(); ?>"><img src="<?php echo $first_image; ?>" alt="" width="100" height="75" /></a><?php endif; ?></td"><td width="340" align="left"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></td><td><a href="<?php the_permalink(); ?>"><img src="/images/photo/bt.jpg" width="109" height="23" /></a></td>
        </tr>
        
				
			

			

<?php endwhile; ?>
</table>

		            </div><!--end  class="wpbox"-->
		          </div><!--end  class="textBox"-->
		        </div><!--end  class="textBg"-->
		      </div><!--end  id="contents"-->
			</div><!-- #content -->
<div class="clr"></div>

<?php get_footer(); ?>

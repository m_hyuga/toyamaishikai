
<?php get_header("josei_tmp"); ?>
       <div class="InnerBlock">
		<div id="MainBlock">

			<?php if (have_posts()) : ?>

			    <?php while (have_posts()) : the_post(); ?>
						<section class="CommonSection">
						<h1><img src="<?php bloginfo('template_url'); ?>/images/josei/ouen/ttl.png" alt="応援メッセージ"　class="alignnone size-full wp-image-5136" /></h1>
							<section class="UnderSection1">
						
						<div class="jyosei_box">
						<div class="content_txt">
						<p class="right_photo"><img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" alt="" /></p>
						<p class="time_page"><?php the_time('Y.m.d'); ?></p>
						<h2 class="ouen"><?php the_title(); ?></h2>
						<h3 class="ouen"><?php echo nl2br(post_custom('fukudai')); ?></h3>
			    	<?php remove_filter('the_content', 'wpautop'); ?>
						<?php the_content(); ?>
						</div>
						</div>
      <div class="page_nav">
			<p class="left01"><?php next_post_link('%link', '戻る', TRUE, ''); ?></p>
      <p class="left02"><a href="/wp/?cat=15"><img src="<?php bloginfo('template_url'); ?>/images/josei/ouen/bt03.jpg" alt="一覧を見る" /></a></p>
			<p class="left03"><?php previous_post_link('%link', '次へ', TRUE, ''); ?></p>
      </div>

  </section>
</section>
			    <?php endwhile; ?>
			<?php endif; ?>

		              	</div>
                        
<?php get_sidebar("josei"); ?>

<?php get_footer("josei"); ?>
﻿<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="keyword" content="医師会,富山,病院,医療情報,健康情報,医療制度,医療費,研修医,女性医師" />
<meta name="Description" content="富山県医師会は、県民の皆さんへよりよい医療・健康情報の提供をおこなっております。" />
<title><?php
	/*
	 * Print the <title> tag based on what is being viewed.
	 */
	global $page, $paged;

	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

	?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link href="/css/common.css" rel="stylesheet" type="text/css"  media="all"/>
<link href="/css/style.css" rel="stylesheet" type="text/css"  media="all"/>


<?php if( in_category('4') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-4.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( in_category('5') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-5.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( in_category('6') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-6.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( in_category('7') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-7.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( in_category('9') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-9.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( in_category('10') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-10.css" rel="stylesheet" type="text/css" media="all" />
<?php else : ?>
<link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" media="all" />

<?php endif; ?>


<?php if(is_page_template('page-4.php')): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-4-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-5.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-5-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-5-2.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-5-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-5_l.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-5-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-6.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-6-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-7.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-7-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-7-2.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-7-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-9.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-9-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-9_l.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-9-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-10.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-10-2.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-11.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-11.css" rel="stylesheet" type="text/css" media="all" />
<?php elseif( is_page_template('page-14.php') ): ?>
<link href="<?php bloginfo('template_directory'); ?>/style-14-2.css" rel="stylesheet" type="text/css" media="all" />
<?php endif; ?>



<script>
  (function() {
    var cx = '012823915592769111243:tshwcnqhzic';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') +
        '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
  })();
</script>


<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
	/* We add some JavaScript to pages with the comment form
	 * to support sites with threaded comments (when in use).
	 */
	if ( is_singular() && get_option( 'thread_comments' ) )
		wp_enqueue_script( 'comment-reply' );

	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
?>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.js"></script>
<script type="text/javascript" src="../js/rollover.js"></script>
<!--Text Resizer start-->
<!--<script type="text/javascript" src="../js/jquery-1.3.2.js"></script>-->
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.cookie.js"></script>
<script type="text/javascript" src="../js/jquery.textresizer.js"></script>

<script>
$(function(){
	$(".textresizer a").textresizer({
		 target: "#container,#footer,#main,.wpbox,.entry-title,.entry-content,.entry-utility,.alignleft,.alignright",
	     sizes:  [ ".7em", "1em","1.3em" ]
	});
});
</script>
<!--Text Resizer end-->

<script type="text/javascript">

 var _gaq = _gaq || [];
 _gaq.push(['_setAccount', 'UA-32709371-1']);
 _gaq.push(['_trackPageview']);

 (function() {
   var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
   ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
   var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
 })();

</script>

</head>

<body <?php body_class(); ?>>
<a name="pagetop" id="pagetop"></a>
<div id="wrapper-bg">
  <div id="wrapper" class="hfeed">
    <div id="header">
      <div class="boxleft">
        <h1><a href="../index.html"><img src="../images/common/logo.jpg" alt="社団法人 富山医師会" width="160" height="54" class="imglnk-no" /></a></h1>
      </div>

<p style="float:right;"><a href="http://www.toyama.med.or.jp/wp/?page_id=6893"><img src="/images/common/papers.gif" alt="各種様式ダウンロードはこちら" width="187" height="55" /></a></p>


      <div class="boxright mr10">
        <p class="textresizer mb5">文字の大きさ ｜ <a href="#">小</a> ｜<a href="#"> 中</a> ｜ <a href="#">大 </a>｜</p>
       <div id="google">
<gcse:searchbox-only></gcse:searchbox-only></div>
 <p style="float:right; margin-top:5px;"><a href="http://www.toyama.med.or.jp/wp/?page_id=1855"><img src="../images/common/bt-member.jpg" alt="会員専用ページはこちら" width="140" height="20" class=" mr25" /></a></p>
<br class="clear">
      </div>
      <br class="clear" />
    </div><!--end id="header"-->
    <div id="container">
      <div id="navi">
        <ul>
          <li><a href="http://www.toyama.med.or.jp/wp/?page_id=2137"><img src="../images/common/navi01_off.jpg" alt="県民の皆様へ" width="240" height="59" /></a></li>
          <li><a href="http://www.toyama.med.or.jp/wp/?page_id=2158"><img src="../images/common/navi02_off.jpg" alt="医療機関の皆様へ" width="240" height="59" /></a></li>
          <li><a href="http://www.toyama.med.or.jp/wp/?page_id=5051"><img src="../images/common/navi03_off.jpg" alt="女性医師等支援相談" width="240" height="59" /></a></li>
          <li><a href="http://www.toyama.med.or.jp/wp/?page_id=2174"><img src="../images/common/navi04_off.jpg" alt="富山県医師会" width="240" height="59" /></a></li>
        </ul>
        <br class="clear" />
      </div><!--end id="navi"-->
